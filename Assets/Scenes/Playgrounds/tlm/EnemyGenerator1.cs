﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;

public class EnemyGenerator1 : MonoBehaviour {
	
	/// <summary> Number of enemies to spawn <\summary>
	public int spawnCount = 1;
	/// <summary> List of the different enemy types <\summary>
	public List<GameObject> enemyType;
    /// <summary> Randomize on Y axis </summary>
    public bool GroundUnit = true;

    /// <summary> </summary>
    private EnemyContainer enemyContainer;
	
	void OnDrawGizmos() {
		Gizmos.DrawWireCube(transform.position, transform.localScale);
	}
	
	/// <summary> 
	/// Generates enemies on startup, using the position 
	/// and scale of the object that this script is attached to
	/// <\summary>
	void Start () 
	{
        enemyContainer = EnemyContainer.GetInstance();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            generateEnemies();
    }

    private void generateEnemies()
    {
        //List<GameObject> existingEnemies = enemyContainer.GetEnemies(); 

		Vector3 boundBoxPosition = transform.position;
		Vector3 boundBoxScale    = transform.localScale;

        // Get the X and Y box boundaries
		float minX = boundBoxPosition.x - boundBoxScale.x * 0.5f;
		float maxX = boundBoxPosition.x + boundBoxScale.x * 0.5f;
        float minY = boundBoxPosition.y - boundBoxScale.y * 0.5f;
		float maxY = boundBoxPosition.y + boundBoxScale.y * 0.5f;

        // Calculate the size of the individual spaces that the enemies spawn in
		float diffX = (maxX - minX) / spawnCount;
		float diffY = (maxY - minY) / spawnCount;

        // The X,Y coordinates for the previous enemy space 
		float prevX = minX;
		float prevY = minY;

        // Create spawnCount number of enemies
		for(int i=0; i<spawnCount; i++)
        {
            // Randomly pick an enemy type
            GameObject chosenEnemy = enemyType[Random.Range(0, enemyType.Count)];

            // Get the size of the enemy
			Vector3 enemySize = chosenEnemy.GetComponent<MeshFilter>().mesh.bounds.min;
			
            float sizeX = enemySize.x * 0.5f;
            float sizeY = enemySize.y * 0.5f;
			Debug.Log(enemySize);

            // Spawn the enemy somewhere in its given space
			//float newX = Random.Range(prevX + sizeX, prevX + diffX - sizeX);
            //float newY = Random.Range(minY + sizeY, maxY - sizeY );
			float newX = prevX + sizeX;
			float newY = prevY + sizeY;
			
			
            // Update prevX/Y with the size of the enemies' spawn space
			prevX = prevX + diffX;
			prevY = prevY + diffY;

            // The spawn position of the enemy
            Vector3 position;
            if(!GroundUnit)
			    position = new Vector3(newX, newY, 1.0f);
            else
                position = new Vector3(newX, minY + sizeY, 1.0f);

            GameObject prespawned = enemyContainer.GetEnemies(chosenEnemy);

            // Create the enemy and put it in the scene
            if(prespawned != null)
            {
                prespawned.gameObject.transform.position = position;
                prespawned.gameObject.transform.rotation = chosenEnemy.transform.rotation;
                prespawned.gameObject.SetActive(true);
            }
            else 
            {
                GameObject g = (GameObject)Instantiate(chosenEnemy, position, chosenEnemy.transform.rotation);
				g.transform.parent = transform;
			}
		}
	}
	
}
