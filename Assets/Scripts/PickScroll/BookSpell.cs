﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BookSpell : MonoBehaviour {
	
	public GameObject BookPage;
    public List<GameObject> EnableGameObjectList;
	public Color fogColor = Color.white;
    public float newSpeed = 1.0f;
    private GlobalSettings.GameplaySettings gameplaySettings;
	
    void Start()
    {
        if (BookPage.GetComponent<PageOnScreen>())
		    BookPage.GetComponent<PageOnScreen>().Book = this;	
        gameplaySettings = GameObject.FindGameObjectWithTag(GlobalSettings.TAG_GAMEMANAGEMENT).GetComponent<GlobalSettings>().gameplaySettings;
	
    }
	
	void OnTriggerEnter(Collider c) {
		
		// Change the player light's color and intensity
        // Change the game speed
        gameplaySettings.PlayerMaxSpeed = newSpeed;

		// Change the fog color
		RenderSettings.fogColor = fogColor;
	 
		
		if (EnableGameObjectList.Count > 0)
        {
            foreach (GameObject go in EnableGameObjectList)
            {
                if (go)
                {
                    go.SetActive(true);
                }
            }
			gameObject.SetActive(false);
			return;
        }
		// Display quad/Spellbook
        if (BookPage)
            BookPage.SetActive(true);


	}
}
