﻿using UnityEngine;
using System.Collections;

public class PageOnScreen : MonoBehaviour {
	
	public Texture2D Image;
	
	[HideInInspector]
	public BookSpell Book;
	
	public GameObject SpellToEnable;
	
	public bool enablesNewSpell = true;

    private PlayerControl player;
	
	private PausableManager pausableManager;
	
	private BackgroundMusic backgroundSound;
		
	void Awake() {
		renderer.material.mainTexture = Image;
        player = GameObject.FindObjectOfType(typeof(PlayerControl)) as PlayerControl;
		backgroundSound = GameObject.FindObjectOfType(typeof(BackgroundMusic)) as BackgroundMusic;
        player.Idle();
	}
	
	void OnEnable() {
		
		//stop player
        pausableManager = GameObject.FindGameObjectWithTag(GlobalSettings.TAG_GAMEMANAGEMENT).GetComponent<PausableManager>();
 		pausableManager.PauseGame();
		backgroundSound.PlayScrollSound();
		backgroundSound.ExitedLevel();
		
		//play spell animation
		//TODO
		
		// pause game
		
		
	}
	
	IEnumerator Animate(){
		//animation code here
		yield return new WaitForSeconds(2.0f);
		//yield WaitForAnimation( animation.PlayQueued( "Intro" ) );	
	}
	
	void OnMouseDown() {
		if (enablesNewSpell)
			SpellToEnable.SetActive(true);
		
		pausableManager.ResumeGame();
        player.Walk();
		
		backgroundSound.EnteredLevel();
        
		Destroy(gameObject);
	}
}
