﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;

public class EnemySounds : MonoBehaviour 
{

	public List<EnemyWithSound> enemies;
	private EnemyWithSound currentEnemy;
	


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void Awake()
	{
		
	}
	
	public void PlayDieSound(EnemyType enemyType)
	{
		foreach( EnemyWithSound enemy in enemies)
		{
			if(enemy.enemyType == enemyType)
			{
				currentEnemy = enemy;
				currentEnemy.PlayDieSound();
			}
		}
	}
	
	public void PlayEnemySound(EnemyType enemyType)
	{
		foreach( EnemyWithSound enemy in enemies)
		{
			if(enemy.enemyType == enemyType)
			{
				currentEnemy = enemy;
				
				currentEnemy.PlayEnemySound();
			}
		}
	}
	
	public void StopEnemySound()
	{
		if(currentEnemy != null)
		{
			currentEnemy.StopEnemySound();
		}
	}
	
	public void PlayEnemyAttackSound(EnemyType enemyType)
	{
		foreach( EnemyWithSound enemy in enemies)
		{
			if(enemy.enemyType == enemyType)
			{
				Debug.Log("sound final");
				currentEnemy = enemy;
				currentEnemy.PlayAttackSound();
			}
		}
	}
	
	public void StopEnemyAttackSound(EnemyType enemyType)
	{
		if(currentEnemy != null)
		{
			currentEnemy.StopAttackSound();
		}
	}
		
}
