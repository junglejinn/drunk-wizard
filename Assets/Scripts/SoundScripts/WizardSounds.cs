﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;

public class WizardSounds : MonoBehaviour {

	public List<SpellWithSound> soundSpells;
	public List<AudioClip> failSounds;
	public AudioClip GameOverSound;
	
	private AudioSource failSound;
	private SpellWithSound selectedSpell;
	private int sequential = 0;
	
	private AudioSource spellSound;
	

	// Use this for initialization
	void Start () {
		failSound = (GetComponents<AudioSource>())[0];
		spellSound = (GetComponents<AudioSource>())[1];
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	public void PlaySpellSound(SpellType spellType)
	{

		
		foreach( SpellWithSound spell in soundSpells)
		{
			if(spell.spell == spellType)
			{
				spellSound.clip = spell.spellSound;
				spellSound.Play();
			}
		}
	}
	
	public void StopSpellSound()
	{

		
		spellSound.Stop();
	}
	
	public void PlayFail()
	{
		
		failSound.clip = failSounds[sequential];
		failSound.Play();
		
		if(failSounds.Capacity - 1 > sequential)
			sequential++;
		else
			sequential = 0;
		
		
	}
	
	public void PlayGameOver()
	{
		failSound.clip = GameOverSound;
		failSound.Play();
	}
	
	
}
