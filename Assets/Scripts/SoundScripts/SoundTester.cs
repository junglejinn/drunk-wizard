﻿using UnityEngine;
using System.Collections;
using Enumerators;

public class SoundTester : MonoBehaviour {
	public GameObject temp;
	private int sequential = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseDown()
	{
		
		switch(sequential)
		{
			case 0:
				temp.GetComponent<WizardSounds>().StopSpellSound();
				temp.GetComponent<WizardSounds>().PlaySpellSound(SpellType.Water);
				sequential++;
				break;
			case 1:
				temp.GetComponent<WizardSounds>().StopSpellSound();
				temp.GetComponent<WizardSounds>().PlaySpellSound(SpellType.Lightning);
				sequential++;
				break;
			case 2:
				temp.GetComponent<WizardSounds>().StopSpellSound();
				temp.GetComponent<WizardSounds>().PlaySpellSound(SpellType.ThunderStorm);
				sequential++;
				break;
			case 3:
				temp.GetComponent<WizardSounds>().StopSpellSound();
				temp.GetComponent<WizardSounds>().PlaySpellSound(SpellType.Failure);
				sequential = 0;
				break;
			
		}
			
		//WizardSounds wizardSounds = transform.parent.GetComponent<WizardSounds>();
		//wizardSounds.PlaySpellSound(SpellType.Lightning);
	
	}
	
	void OnMouseUp()
	{
		
	}
}
