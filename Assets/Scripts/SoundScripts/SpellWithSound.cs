﻿using UnityEngine;
using System.Collections;
using Enumerators;
[System.Serializable]
public class SpellWithSound {
	
	public SpellType spell;

	public AudioClip spellSound;
	
}
