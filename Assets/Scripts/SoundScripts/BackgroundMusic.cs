﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackgroundMusic : MonoBehaviour {
	
    public float backgroundVolume;

    public List<LvlBackgroundMusic> lvlBackgroundMusic;
	public AudioSource backgroundEffects;
	public AudioSource music;
    [System.Serializable]
    public class LvlBackgroundMusic
    {
        public int level;
        public AudioClip start;
        public AudioClip loop;
    }

    private int currentLevel = 1;


    //public AudioLowPassFilter lowPassFilter;
    
    //public float cutoffFrequencyMin;
    //public float cutoffFrequencyStart;
    //public float cutoffFrequencyMax;
    //public float lowpassResonaceQ;
    //public float delay = 10f;

    //void Awake()
    //{
    //    //lowPassFilter.cutoffFrequency = cutoffFrequencyStart;
    //    //lowPassFilter.lowpassResonaceQ = lowpassResonaceQ;

    //    if (backgroundMusic != null)
    //    {
    //        backgroundMusic.volume = backgroundVolume;
    //        backgroundMusic.Play();
    //        //backgroundMusic.Stop ();
    //    }
    //}

	// Use this for initialization
	void Start () 
	{
        music.volume = 1;
        music.clip = lvlBackgroundMusic.Find(item => item.level == 1).start;
        music.Play();

        StartCoroutine(PlayLoopAfter(music.clip.length));
	}

    IEnumerator PlayLoopAfter(float t)
    {
        yield return new WaitForSeconds(t);

        music.clip = lvlBackgroundMusic.Find(item => item.level == currentLevel).loop;
        music.loop = true;
        music.Play();
    }

    public void ExitedLevel()
    {
        music.Stop();
    }

    public void EnteredLevel()
    {
        ++currentLevel;
        music.clip = lvlBackgroundMusic.Find(item => item.level == currentLevel).start;
        music.loop = false;
        music.Play();

        StartCoroutine(PlayLoopAfter(music.clip.length));
    }
	
	public void PlayScrollSound()
	{
		backgroundEffects.Play();
	}
	
	public IEnumerator PlayerHitSoundEffect()
	{
		//Code called when changing the frequencty. 
		
        //Debug.Log ("Sound effects called");
        //lowPassFilter.cutoffFrequency = cutoffFrequencyMin;
        //yield return new WaitForSeconds (delay);
        //lowPassFilter.cutoffFrequency = cutoffFrequencyMax;


        yield return null;
		
	}
	

    //void ReduceCutoffFrequency(float delta)
    //{
			
    //    //Code for instant change in frequency.
		
    //    float newCutoffFrequency = lowPassFilter.cutoffFrequency - delta;
    //    if(newCutoffFrequency >= cutoffFrequencyMin)
    //    {
    //        lowPassFilter.cutoffFrequency = newCutoffFrequency;
    //    }
    //    else
    //    {
    //        lowPassFilter.cutoffFrequency = cutoffFrequencyMin;
    //    }
		
    //}
	
    //void IncreaseCutoffFrequency(float delta)
    //{

    //    //Code for instant change in frequency.
		
    //    float newCutoffFrequency = lowPassFilter.cutoffFrequency + delta;
    //    if(newCutoffFrequency <= cutoffFrequencyMax)
    //    {
    //        lowPassFilter.cutoffFrequency = newCutoffFrequency;
    //    }
    //    else
    //    {
    //        lowPassFilter.cutoffFrequency = cutoffFrequencyMax;
    //    }
    //}
}
