﻿using UnityEngine;
using System.Collections;
using Enumerators;
[System.Serializable]
public class EnemyWithSound  
{
	public EnemyType enemyType;
	public AudioSource enemySound;
	public AudioSource attackSound;
	public AudioSource dieSound;
	
	public void PlayEnemySound()
	{
		//enemySound.loop = true;
		enemySound.Play();
	}
	
	public void PlayAttackSound()
	{
		
		attackSound.Play();
	}
	
	public void StopEnemySound()
	{
		enemySound.Stop();
	}
	
	public void StopAttackSound()
	{
		enemySound.Stop();
	}
	
	public void PlayDieSound()
	{
		dieSound.Play ();
	}
	

}
