﻿using UnityEngine;
using System.Collections;
using Enumerators;

public class MainMenu : MonoBehaviour {
	
	public GameObject creditQuad;
	
	void Start(){
		Screen.sleepTimeout = (int) SleepTimeout.NeverSleep;	
	}
	
	public void ButtonPressed(MainMenuButtonType button){
		switch(button){
			case MainMenuButtonType.StartGame:
				Application.LoadLevel("Wizard-Game");
			break;
			case MainMenuButtonType.ExitGame:
				Application.Quit();
			break;
			case MainMenuButtonType.Credits:
				creditQuad.SetActive(true);
			break;
			case MainMenuButtonType.Return:
				creditQuad.SetActive(false);
			break;
			
		}
	}
}
