﻿using UnityEngine;
using System.Collections;

public class VictoryScreen : MonoBehaviour {
	
	public Material fadeTo;
	private Color materialColor;
	
	private GameStateManager gameStateManager;
	
	void Start () {
		materialColor = renderer.material.color;
		renderer.material.color = new Color(materialColor.r, materialColor.g, materialColor.b, 0f);
		StartCoroutine(FadeTo(1f, 1f));
		gameStateManager = GameObject.FindGameObjectWithTag(GlobalSettings.TAG_GAMEMANAGEMENT).GetComponent<GameStateManager>();
	}
	
	IEnumerator FadeTo(float aValue, float aTime)
	{
	    float alpha = transform.renderer.material.color.a;
	    for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
	    {
	        Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha,aValue,t));
	        transform.renderer.material.color = newColor;
	        yield return null;
	    }
	}
	
	void OnMouseDown()
	{
		gameStateManager.ChangeGameState(Enumerators.GameState.MainMenu);
		GameObject.Destroy(this.gameObject);
	}
}
