﻿using UnityEngine;
using System.Collections;

public class Pause : IPausable {
	
	PausableManager pausableManager;
	
	// Use this for initialization
	void Start () {
	
		pausableManager = GameObject.FindGameObjectWithTag(GlobalSettings.TAG_GAMEMANAGEMENT).GetComponent<PausableManager>();
	}
	
	void OnMouseDown()
	{
		if (isPaused)
			pausableManager.ResumeGame();
		else	
			pausableManager.PauseGame();
	}
}
