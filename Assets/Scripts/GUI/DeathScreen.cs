﻿using UnityEngine;
using System.Collections;
using Enumerators;

public class DeathScreen : MonoBehaviour {
	
	private GameStateManager gameStateManager;
	
	void Start () {
		gameStateManager = GameObject.FindGameObjectWithTag(GlobalSettings.TAG_GAMEMANAGEMENT).GetComponent<GameStateManager>();
	}
	
	void OnMouseDown()
	{
		gameStateManager.ChangeGameState(Enumerators.GameState.MainMenu);
		GameObject.Destroy(this.gameObject);
	}
}
