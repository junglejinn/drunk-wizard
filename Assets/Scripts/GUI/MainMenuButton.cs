﻿using UnityEngine;
using System.Collections;
using Enumerators;

public class MainMenuButton : MonoBehaviour {
	
	public MainMenuButtonType type;
	private MainMenu menu;
	
	void Awake() {
		menu = (MainMenu)GameObject.FindObjectOfType(typeof(MainMenu));
	}
	
	void OnMouseDown(){
		menu.ButtonPressed(type);
	}
	
}
