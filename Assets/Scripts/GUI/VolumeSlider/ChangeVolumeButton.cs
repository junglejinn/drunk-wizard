﻿using UnityEngine;
using System.Collections;

public class ChangeVolumeButton : MonoBehaviour 
{
	//Audio source for which I wish to change the volume.
	//public AudioSource audioSource;
	
	public int ChangeDirection;
	
	private VolumeChanger volumeChanger;
	
	// Use this for initialization
	void Start () {
		// get reference to the volume changer
		volumeChanger = transform.parent.GetComponent<VolumeChanger>();
	}
	
	void OnMouseDown()
	{
		volumeChanger.ChangeVolume(ChangeDirection);
	}
	
	void OnMouseUp()
	{
		volumeChanger.ChangeVolume (0);
	}
	
	
	
}
