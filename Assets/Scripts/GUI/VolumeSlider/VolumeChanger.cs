﻿using UnityEngine;
using System.Collections;

public class VolumeChanger : MonoBehaviour {
	
	public float Volume;
	public float Speed;
	
	private VolumeSlider slider;
	private ChangeVolumeButton buttons;
	private float change;
	
	void Start()
	{
		slider = transform.GetComponentInChildren<VolumeSlider>();
		buttons =transform.GetComponentInChildren<ChangeVolumeButton>();
		SetSliderPosition();
		
	}
	
	public void ChangeVolume(float change)
	{
		this.change = change;
	}
	
	
	void Update()
	{
		if(change != 0)
		{
			// change volume
		AudioListener.volume += change * Speed;
		
		// clamp values
		if (AudioListener.volume > 1)
			AudioListener.volume = 1;
		else if (AudioListener.volume < 0)
			AudioListener.volume = 0;
		
		// move slider
		SetSliderPosition();
		Debug.Log(AudioListener.volume);
		}
	}
	
	void SetSliderPosition()
	{
		Debug.Log("set");
		Vector3 button = buttons.gameObject.transform.position;
		float x = AudioListener.volume*10;
		Vector3 temp = new Vector3(button.x+(x+1),button.y,button.z);
		slider.gameObject.transform.position = temp;
		
	}

}
