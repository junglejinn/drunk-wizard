﻿using UnityEngine;
using System.Collections;
using Enumerators;

public class SpellButton : IPausable
{
    public SpellType ButtonType;
	public bool isCancel = false;
	public PlayerLogic player;
	
	// size of the sprite sheet column and rows
	public int colCount 	=  4;
	public int rowCount 	=  4;
	 
	//varibels for animation
	public int  startRowNumber  	= 0;  	//Zero Indexed
	public int  startColNumber 		= 0;  	//Zero Indexed
	public int  totalCells 			= 16; 	// The number of cells the sprite animation goes through starting from startRow and StartCol
	public int framesPerSecond		= 1;
	public static int  fps     		= 1;  	// frame per second - e.g number of cells per second
	
	private Vector2 offset; 				// the offset of texture coodinates
	private int index;						// the frame/cell which is being shown
	private static int myTimeFactor = 1; 	// currently not working
	
	private static bool stopped;
	private float startTime;
	private static bool playSprite = false;
	
	void Awake() {
		player = (PlayerLogic)GameObject.FindObjectOfType(typeof(PlayerLogic));		
	}
	
	void Update()
	{
		
		if(index <= 16)
		{
			stopped = false;
		}
		
		SetSpriteAnimationOnce(colCount,rowCount,startRowNumber,startColNumber,totalCells,framesPerSecond);
	}
	
	void Start()
	{
		startTime = Time.time;
	}
	
    public override void Pause()
    {
        isPaused = true;
    }

    public override void Resume()
    {
        isPaused = false;
    }
	
	//SetSpriteAnimation
	public void SetSpriteAnimation(int colCount ,int rowCount ,int startRowNumber ,int startColNumber,int totalCells,int fps)
	{
		// Calculate index
	    index  = (int)(Time.time * fps);

	    // Repeat when exhausting all cells
	    index = index % totalCells;
	    
		// Size of every cell
	    float sizeX = 1.0f / colCount;
	    float sizeY = 1.0f / rowCount;
	    Vector2 size =  new Vector2(sizeX,sizeY);
	 
	    // split into horizontal and vertical index
	    var uIndex = index % colCount;
	    var vIndex = index / colCount;
		
	    // build offset
	    // v coordinate is the bottom of the image in opengl so we need to invert.
	    float offsetX = (uIndex+startColNumber) * size.x;
	    float offsetY = (1.0f - size.y) - (vIndex + startRowNumber) * size.y;
	    Vector2 offset = new Vector2(offsetX,offsetY);
	 
	    renderer.material.SetTextureOffset ("_MainTex", offset);
	    renderer.material.SetTextureScale  ("_MainTex", size);
		
		stopped = index == (totalCells - 1);  // last index
		//Debug.Log ("index = " + index + "totalCells = " + totalCells + "stopped = " + stopped);
	}
	
	public void SetSpriteAnimationOnce(int colCount ,int rowCount ,int startRowNumber ,int startColNumber,int totalCells,int fps)
	{
		if(!stopped)
		{
			index = (int) Mathf.Min(colCount*rowCount - 1, (Time.time-startTime)* fps);
			stopped = index == colCount * rowCount - 1;

	    	// Repeat when exhausting all cells
	    	//index = index % totalCells;
			//stopped = index == (totalCells - 1);  // last index
			
			// Size of every cell
		    float sizeX = 1.0f / colCount;
		    float sizeY = 1.0f / rowCount;
		    Vector2 size =  new Vector2(sizeX,sizeY);
		 
		    // split into horizontal and vertical index
		    var uIndex = index % colCount;
		    var vIndex = index / colCount;
			
		    // build offset
		    // v coordinate is the bottom of the image in opengl so we need to invert.
		    float offsetX = (uIndex+startColNumber) * size.x;
		    float offsetY = (1.0f - size.y) - (vIndex + startRowNumber) * size.y;
		    Vector2 offset = new Vector2(offsetX,offsetY);
		 
		    renderer.material.SetTextureOffset ("_MainTex", offset);
		    renderer.material.SetTextureScale  ("_MainTex", size);
		}
	}
	
    void OnMouseDown()
    {
		startTime = Time.time;
		//index = 0;
		// hello
        if (!isPaused)
        {
			if(isCancel){
				player.ClearSpells();
			} else {
	            player.AddSpell(ButtonType);
			}
        }
    }	
}
