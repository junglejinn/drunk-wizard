﻿using UnityEngine;
using System.Collections;

public class ShadowMovement : MonoBehaviour {
	
	private Vector3 tosub;
	
	// Use this for initialization
	void Start () {
		tosub = new Vector3(0.0f, 0.0f, 0.0f);
	}
	
	// Update is called once per frame
	void Update () {
		tosub.x = transform.position.x;
		tosub.z = transform.position.z;
		transform.position = tosub;
	}
}
