﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This namespace should contain all enumerators.
/// </summary>

namespace Enumerators
{
	
	public enum MainMenuButtonType
    {
        StartGame,
		ExitGame,
		Credits,
		Return
    };
	
    public enum GameState
    {
        RunningGame,
        PausedGame,
        BossBattle,
		PlayerDeath,
		MainMenu,
		SpellBook,
		PlayerWin,
        Quit,
		None
    };
		
    public enum SpellType
    {
        Water = 0,
        Lightning = 1,
        Fire = 2,
        ThunderStorm, // Water & Lightning combined
        Failure
    };


    public enum EnemyType
    {
        Broom,
        EvilBroom,
        RedDragon,
        MetalDragon,
        DarkWizard
    };
}

