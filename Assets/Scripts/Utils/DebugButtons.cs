﻿using UnityEngine;
using System.Collections;

public class DebugButtons : MonoBehaviour {

    PausableManager pausableManager;

    void Start()
    {
        pausableManager = GameObject.FindGameObjectWithTag(GlobalSettings.TAG_GAMEMANAGEMENT).GetComponent<PausableManager>();
    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 75, 100), "Pause"))
        {
            pausableManager.PauseGame();
        }

        if (GUI.Button(new Rect(75, 0, 75, 100), "Resume"))
        {
            pausableManager.ResumeGame();
        }
		if (GUI.Button(new Rect(150, 0, 75, 100), "Restart"))
        {
            Application.LoadLevel(0);
        }
		if (GUI.Button(new Rect(225, 0, 75, 100), "Quit"))
        {
			Application.Quit();
        }
    }
}
