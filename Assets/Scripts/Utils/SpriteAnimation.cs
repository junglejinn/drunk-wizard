﻿using UnityEngine;
using System.Collections;

public class SpriteAnimation: MonoBehaviour {
	
	// size of the sprite sheet column and rows
	public int colCount 	=  4;
	public int rowCount 	=  4;
	 
	//varibels for animation
	public int  startRowNumber  	= 0;  //Zero Indexed
	public int  startColNumber 		= 0;  //Zero Indexed
	public int  totalCells 			= 16; // The number of cells the sprite animation goes through starting from startRow and StartCol
	public int  fps     			= 1;  // frame per second - e.g number of cells per second
	
	private Vector2 offset; 				// the offset of texture coodinates
	private static bool playSprite = true; 	// bool to check if sprite should play
	private int index;						// the frame/cell which is being shown
	
	public bool showSprite = true;			// playing is true
	
	//Update
	void Update() 
	{
		if(showSprite)
		{
			SetSpriteAnimation(colCount,rowCount,startRowNumber,startColNumber,totalCells,fps);
		}else
		{
			renderer.enabled = false;
		}
	}
	 
	//SetSpriteAnimation
	public void SetSpriteAnimation(int colCount ,int rowCount ,int startRowNumber ,int startColNumber,int totalCells,int fps)
	{
		
		// Calculate index
	  	index  = (int)(Time.time * fps);
		
	    // Repeat when exhausting all cells
	    index = index % totalCells;
		
	    // Size of every cell
	    float sizeX = 1.0f / colCount;
	    float sizeY = 1.0f / rowCount;
	    Vector2 size =  new Vector2(sizeX,sizeY);
	 
	    // split into horizontal and vertical index
	    var uIndex = index % colCount;
	    var vIndex = index / colCount;
		
	    // build offset
	    // v coordinate is the bottom of the image in opengl so we need to invert.
	    float offsetX = (uIndex+startColNumber) * size.x;
	    float offsetY = (1.0f - size.y) - (vIndex + startRowNumber) * size.y;
	    Vector2 offset = new Vector2(offsetX,offsetY);
	 
	    renderer.material.SetTextureOffset ("_MainTex", offset);
	    renderer.material.SetTextureScale  ("_MainTex", size);
	}
	
	
	// for debug
	/*void OnGUI()
	{	
		if(GUI.Button(new Rect(975, 0, 100, 100),"Stop Sprite"))
		{
			myTimeFactor = 0;
			playSprite = false;
		}
		
		if(GUI.Button(new Rect(1075,0,100,100), "Resume Sprite"))
		{
			myTimeFactor = 1;
			playSprite = true;
			
		}
	}*/
}
