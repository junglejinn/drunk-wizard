﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Outro : MonoBehaviour {
	// The textures for the outro pages.
	public List<Texture> outroPages = new List<Texture>();
	// The textures for the credit pages.
	public List<Texture> creditPages = new List<Texture>();
    public float waitPerFlip = 5.0f;
	public float fadeInTime = 1f;
	public float waitBetweenSequences = 3f;
	public bool ChangeSceneAfterOutro = true;
	
	private Color materialColor;
	
	// Use this for initialization
	void Start () {
		materialColor = renderer.material.color;
		renderer.material.color = new Color(materialColor.r, materialColor.g, materialColor.b, 0f);
		StartCoroutine(FadeTo(1f, fadeInTime));
		StartCoroutine(Flip());
		
	}
	
	IEnumerator Flip() 
	{
		yield return new WaitForSeconds(waitPerFlip);
			
        foreach (Texture outroPage in outroPages)
        {
            renderer.material.mainTexture = outroPage;
            yield return new WaitForSeconds(waitPerFlip);
        }
		
		yield return new WaitForSeconds(3f); // Wait additional time before starting on the outro.
		
		foreach (Texture creditPage in creditPages)
        {
            renderer.material.mainTexture = creditPage;
            yield return new WaitForSeconds(waitPerFlip);
        }
		
		if (ChangeSceneAfterOutro)
			Application.LoadLevel("Wizard-Menu");
	}
	
	IEnumerator FadeTo(float aValue, float aTime)
	{
	    float alpha = transform.renderer.material.color.a;
	    for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
	    {
	        Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha,aValue,t));
	        transform.renderer.material.color = newColor;
	        yield return null;
	    }
	}
	
	void OnMouseDown()
	{
		Application.LoadLevel("Wizard-Menu");
	}
}
