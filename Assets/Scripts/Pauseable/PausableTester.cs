﻿using UnityEngine;
using System.Collections;

public class PausableTester : IPausable
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Pause()
    {
        particleSystem.Pause();
    }

    public override void Resume()
    {
        particleSystem.Play();
    }}
