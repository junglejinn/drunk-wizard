﻿using UnityEngine;
using System.Collections;

public class IPausable : MonoBehaviour
{
    public virtual void Pause() { isPaused = true; }
    public virtual void Resume() { isPaused = false; }

    protected bool isPaused;
}