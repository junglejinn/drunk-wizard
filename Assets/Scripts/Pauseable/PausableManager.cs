using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PausableManager : MonoBehaviour
{
    private List<IPausable> pausables;
    private bool isPaused;

    void Awake()
    {
	    pausables = new List<IPausable>();
        pausables.AddRange(GameObject.FindObjectsOfType(typeof(IPausable)) as IPausable[]);

        isPaused = false;
    }

    public void PauseGame()
    {
        if (pausables == null || isPaused) return;
		
        foreach (IPausable p in pausables)
        {
			if (p)
            	p.Pause();
        }

        isPaused = true;
    }

    public void ResumeGame()
    {
        if (!isPaused) return;

        foreach (IPausable p in pausables)
        {
            p.Resume();
        }

        isPaused = false;
    }

    public void RegisterIPausable(IPausable newPausable)
    {
        pausables.Add(newPausable);
    }

    public void UnregisterIPausable(IPausable oldPausable)
    {
        pausables.Remove(oldPausable);
    }
}

