﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;

public class PlayerLogic : MonoBehaviour
{
    /// <summary>
    /// Determines how many spells the Player can have queued at a time.
    /// </summary>
    public int MaximumNumberOfSpells = 3;
	/// <summary> Determines for how long the wizard is invulnerable after getting hit</summary>
	public float InvulnerabilityTime = 1.5f;
    public int Health { get; private set; }

	public float healthRegainTimeout = 10;
	private float lastHitTime;

    private SpellConverter spellConverter;
	private SpellBubbles spellBubbles;
	private PlayerControl playerControl;
    private PlayerMovement playerMovement;
	private GameStateManager gameStateManager;

    private Transform playerHearts;

    private List<SpellType> currentSpells = new List<SpellType>();
	
	//Variable which contains all the sounds for the wizard.
	private WizardSounds wizardSounds;
	
	private ParticleSpawner particleSpawner; 
	
	private bool isInvulnerable = false;
	
	public bool debugInVulnerable = false;
	
    // Use this for initialization
    void Start()
    {
        spellConverter = GameObject.FindGameObjectWithTag(GlobalSettings.TAG_GAMEMANAGEMENT).GetComponent<SpellConverter>();
		spellBubbles = GetComponent<SpellBubbles>();
		playerControl = GetComponent<PlayerControl>();
        playerMovement = GetComponent<PlayerMovement>();
		gameStateManager = GameObject.FindGameObjectWithTag(GlobalSettings.TAG_GAMEMANAGEMENT).GetComponent<GameStateManager>();
        playerHearts = GameObject.FindGameObjectWithTag(GlobalSettings.TAG_PLAYERHEARTS).transform;
		wizardSounds =GameObject.FindObjectOfType(typeof(WizardSounds)) as WizardSounds;
		particleSpawner = gameObject.GetComponent<ParticleSpawner>();
        Health = 3;
    }

	void Update()
	{
		if (Health < 3 && Time.time - lastHitTime > healthRegainTimeout) 
		{
			playerHearts.FindChild("PlayerHeart" + Health).gameObject.SetActive(false);
			Health++;
			// only gain one health point each regain period
			lastHitTime = Time.time;
			playerHearts.FindChild("PlayerHeart" + Health).gameObject.SetActive(true);
		}
	}

    /// <summary>
    /// Adds the spell.
    /// </summary>
    /// <param name='spellType'>
    /// Spell type.
    /// </param>
    public void AddSpell(SpellType spell)
    {
        if (currentSpells.Count < MaximumNumberOfSpells)
            currentSpells.Add(spell);
        else if (currentSpells.Count == MaximumNumberOfSpells)
        {
            // If the Player has already reached the maxmimum number of spells,
            // remove the oldest spell and add the new spell.
            currentSpells.RemoveAt(0);
            currentSpells.Add(spell);
        }

		// Update spellBubbles
		spellBubbles.UpdateSpellBubbles(currentSpells);
    }

    /// <summary>
    /// Remove all current spells from the Player
    /// </summary>
    public void ClearSpells()
    {
        currentSpells.Clear();
		spellBubbles.UpdateSpellBubbles(currentSpells);
    }
	
	private bool spellClash;
    public void ShootSpells(BaseEnemy enemy)
    {
	
        if (currentSpells.Count == 0)
            return;
		
		playerControl.Attack(); // Play 'Attack'-Animation
		
        List<SpellType> spells = new List<SpellType>();
        if (GameStateManager.CurrentState == GameState.RunningGame)
        {
            // check if spells are a valid combination
            spells = spellConverter.Convert(currentSpells);
        }
        else if (GameStateManager.CurrentState == GameState.BossBattle)
        {
            spells.AddRange(currentSpells);
        }
		
		
        bool didHit = enemy.Attacked(spells);
		
		if(didHit)
		{
			wizardSounds.PlaySpellSound(spells[0]);
		}
		else
		{
			wizardSounds.PlayFail();
		}

		
		if(spellClash){
			particleSpawner.SpawnParticles(spellClash, didHit, decideCombinedParticleEffect(spells), enemy.transform.FindChild("0"));
		}
		else if(didHit && GameStateManager.CurrentState == GameState.BossBattle){
			particleSpawner.SpawnParticles(spellClash, didHit, spells, enemy.transform.FindChild("0"));
		}
		else{
			particleSpawner.SpawnParticles(spellClash, didHit, spells, enemy.transform.FindChild("0"));
			
		}
		spellClash = false;
		
		//Debug.Log ("++++++++++++++++ Shoot Spells;");
		// After shooting, we need to clear spells and Update the spellbubbles.
		ClearSpells();
    }
	
	private List<SpellType> decideCombinedParticleEffect( List<SpellType> spells ){
		int lightningCount = 0;
		int waterCount = 0;
		for(int i=0; i< spells.Count; i++){
			if( spells[i] == SpellType.Lightning ){
				lightningCount++;
			} else if (spells[i] == SpellType.Water){
				waterCount++;
			}
		}
		
		spells.Clear();
		
		if(waterCount == 0){
			spells.Add(SpellType.Lightning);	
		} else if(lightningCount == 0){
			spells.Add(SpellType.Water);	
		} else if(lightningCount > waterCount+1){
			spells.Add(SpellType.Lightning);
		} else if(waterCount > lightningCount+1){
			spells.Add(SpellType.Water);
		} else 
			spells.Add(SpellType.ThunderStorm);
		
		return spells;
		
	}

    /// <summary>
    /// Call this when the player gets hit
    /// </summary>
    public void GetHit()
    {
		if(debugInVulnerable)
			return;
		
		if (!isInvulnerable)
		{
			playerControl.GetHit(); // Play 'Got hit'-animation.
			
			playerHearts.FindChild("PlayerHeart" + Health).gameObject.SetActive(false);
        	Health--;
			lastHitTime = Time.time;

            currentSpells.Clear();
            spellBubbles.UpdateSpellBubbles(currentSpells);
	
			//Debug.Log ("++++++++++++++++ WizHit;");///first you shoot then you get hit.
			
	        if (Health <= 0)
	        {
	            StartCoroutine(DieAfter(1));
	        }
	
	        if (playerHearts)
			{
				playerHearts.FindChild("PlayerHeart" + Health).gameObject.SetActive(true);
				StartCoroutine(MakeInvulnerable());
			}
		}
    }

    IEnumerator DieAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
		gameStateManager.ChangeGameState(GameState.PlayerDeath);
		wizardSounds.PlayGameOver();
		
        GA.API.Design.NewEvent("Player died in level " + GameStateManager.CurrentLevel + " at time (in level) and position", Time.timeSinceLevelLoad, transform.position);

        if (GameStateManager.CurrentState == GameState.BossBattle)
            GA.API.Design.NewEvent("Player died in boss battle, at time (since level load)", Time.timeSinceLevelLoad); 
    }
	
	/// <summary> A coroutine for making the wizard temporarily IMMORTAL <summary>
	IEnumerator MakeInvulnerable()
	{
		isInvulnerable = true;
      	yield return new WaitForSeconds(InvulnerabilityTime);
		isInvulnerable = false;
	}



	public void QueuePlayerForGettingHit(BossApprenticeEnemy enemy)
    {
		//Debug.Log ("++++++++++++++++ Apprentice attacks you;");//first iy gets interrupted then you shoot
		//enemy casts his spells here on you
		//particleSpawner.SpawnParticles(false, true, decideCombinedParticleEffect(enemy.VulnerableTo), playerControl.spellCastOrigin.transform);
		StartCoroutine( DelayParticles(enemy, decideCombinedParticleEffect(enemy.VulnerableTo), playerControl.spellCastOrigin.transform) );
	}
	
	private IEnumerator DelayParticles (BossApprenticeEnemy enemy, List<Enumerators.SpellType> spell, Transform position)

    {

        yield return new WaitForSeconds(1.1f);

        particleSpawner.SpawnParticles(false, true, spell, position);

    }

    public void QueuePlayerBossClash(BossApprenticeEnemy enemy)
    {
		//Debug.Log ("++++++++++++++++ Apprentice interrupted;");//first he gets interrupted then you shoot
		//set flag for clash spell here. to becalled in the ShootSpells function.
		spellClash = true;
    }

    public void StartMoving()
    {
        playerMovement.enabled = true;
        playerControl.Walk();
    }

    public void StopMoving()
    {
        playerMovement.enabled = false;
        playerControl.Idle();
    }
}
