﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {
	
	public GameObject spellCastOrigin;
	
	private Animator animator;	// a reference to the animator on the character
	private AnimatorStateInfo currentBaseState; // a reference to the current state of the animator

	private const string ATTACKING = "Attacking";
	private const string IDLE = "Idle";
	private const string HIT = "Hit";
	
	private static int attackState = Animator.StringToHash("Base Layer.Attacking");		
	private static int hitState = Animator.StringToHash("Base Layer.Hit");	
	private static int idleState = Animator.StringToHash("Base Layer.Idle");
	private static int walkState = Animator.StringToHash("Base Layer.Walking");	
	
	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
	}
	
		
	void FixedUpdate ()
	{
		currentBaseState = animator.GetCurrentAnimatorStateInfo(0);	// set our currentState variable to the current state of the Base Layer (0) of animation
		
		if (currentBaseState.nameHash == hitState && !animator.IsInTransition(0))
		{
			animator.SetBool(HIT, false);
		}
		
		if (currentBaseState.nameHash == attackState && !animator.IsInTransition(0))
		{
			animator.SetBool(ATTACKING, false);
		}
	}
	
	public void Attack()
	{
		animator.SetBool(ATTACKING, true);
	}
	
	public void Walk()
	{
		animator.SetBool(IDLE, false);
	}
	
	public void Idle()
	{
		animator.SetBool(IDLE, true);
	}
	
	public void GetHit()
	{
		animator.SetBool(HIT, true);
	}
	

}
