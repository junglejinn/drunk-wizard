﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;
using System;

public class SpellBubbles : MonoBehaviour
{
    public GameObject prefabBubbleParent;
    /// <summary> The (prefab) SpellBubble that should be used as bubbles above the player. </summary>
    public GameObject prefabBubble;
    /// <summary> The amount of SpellBubbles available to the player</summary>
    public int amountOfBubbles = 2;
    /// <summary> The left offset as a 2D vector. </summary>
    public Vector3 leftOffset = new Vector3(0, 0, 0);
    /// <summary> Determines the distances between the spell bubbles</summary>
    public float distanceBetweenSpellBubbles = 1;
    /// <summary> The Material used for the water SpellBubble. </summary>
    public Material Water;
    /// <summary> The Material used for the lightning SpellBubble. </summary>
    public Material Lightning;

    Dictionary<GameObject, GameObject> spellBubbles = new Dictionary<GameObject, GameObject>();

    /// <summary>
    /// Clears current spellBubbles and creates new spellBubbles based on the spells given.
    /// </summary>
    /// <param name='spells'>
    /// The list of spells.
    /// </param>
    public void UpdateSpellBubbles(List<SpellType> spells)
    {
        List<SpellType> tempSpells = new List<SpellType>();
        tempSpells.AddRange(spells);
        foreach (GameObject spellBubble in spellBubbles.Values)
        {
            if (tempSpells.Count > 0)
            {
                SpellType spell = tempSpells[0]; 
                tempSpells.RemoveAt(0);

                spellBubble.renderer.enabled = true;
                SetSpellBubbleMaterial(spellBubble, spell);
            }
            else
                spellBubble.renderer.enabled = false;
        }
    }

    private void SetSpellBubbleMaterial(GameObject spellBubble, SpellType spell)
    {
        switch (spell)
        {
            case SpellType.Water:
                spellBubble.renderer.material = Water;
                break;
            case SpellType.Lightning:
                spellBubble.renderer.material = Lightning;
                break;
            default:
                break;
        }

        if (spellBubble.transform.position.x < transform.position.x)
            spellBubble.renderer.material.mainTextureScale = new Vector2(-1, 1);
    }

    void Awake()
    {
        // Instantiate SpellBubbles.
        Vector3 playerPos = transform.position;
        Quaternion playerRot = transform.rotation;

        float currentXOffset = leftOffset.x;
        for (int i = 0; i < amountOfBubbles; i++)
        {
            if (i > 0)
                currentXOffset += distanceBetweenSpellBubbles;
            GameObject spellBubbleParent =
                Instantiate(prefabBubbleParent, new Vector3(playerPos.x + currentXOffset, playerPos.y + leftOffset.y, playerPos.z + leftOffset.z), playerRot) as GameObject;
            GameObject spellBubble =
                Instantiate(prefabBubble, new Vector3(playerPos.x + currentXOffset, playerPos.y + leftOffset.y, playerPos.z + leftOffset.z), playerRot) as GameObject;
            spellBubbles.Add(spellBubbleParent, spellBubble);
        }

        foreach (KeyValuePair<GameObject, GameObject> spellBubble in spellBubbles)
        {
            spellBubble.Key.transform.parent = transform;

            // Apply settings to the (actual) spell bubble
            spellBubble.Value.transform.parent = spellBubble.Key.transform;
            spellBubble.Value.renderer.enabled = false;
            spellBubble.Value.animation["SpellBubbleAnimation"].time = UnityEngine.Random.Range(0.0f, spellBubble.Value.animation["SpellBubbleAnimation"].length);
            if (spellBubble.Key.transform.position.x > playerPos.x)
                spellBubble.Value.renderer.material.mainTextureScale = new Vector2(-1, 1);
        }
    }
}
