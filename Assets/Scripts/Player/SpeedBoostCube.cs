﻿using UnityEngine;
using System.Collections;

public class SpeedBoostCube : MonoBehaviour {
	
	public float newSpeed = 1.0f;
    private GlobalSettings.GameplaySettings gameplaySettings;

    void Start()
    {
        gameplaySettings = GameObject.FindGameObjectWithTag(GlobalSettings.TAG_GAMEMANAGEMENT).GetComponent<GlobalSettings>().gameplaySettings;
	
    }
	
	void OnTriggerEnter(Collider c) {
		gameplaySettings.PlayerMaxSpeed = newSpeed;
	}
}
