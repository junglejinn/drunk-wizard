﻿using UnityEngine;
using System.Collections;

public class EnemyHitTrigger : MonoBehaviour
{
    PlayerLogic logic;
    private BackgroundMusic hitSoundEffect;
	private EnemySounds enemySounds;
	private WizardSounds wizardSounds;

    void Start()
    {
        logic = GameObject.FindObjectOfType(typeof(PlayerLogic)) as PlayerLogic;
		hitSoundEffect = GameObject.FindObjectOfType(typeof(BackgroundMusic)) as BackgroundMusic;
		enemySounds = GameObject.FindObjectOfType(typeof(EnemySounds))as EnemySounds;
		wizardSounds = GameObject.FindObjectOfType(typeof(WizardSounds))as WizardSounds;
    }
	
	void Awake()
	{
		logic = GameObject.FindObjectOfType(typeof(PlayerLogic)) as PlayerLogic;
		hitSoundEffect = GameObject.FindObjectOfType(typeof(BackgroundMusic)) as BackgroundMusic;
		enemySounds = GameObject.FindObjectOfType(typeof(EnemySounds))as EnemySounds;
		wizardSounds = GameObject.FindObjectOfType(typeof(WizardSounds))as WizardSounds;
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == GlobalSettings.TAG_ENEMY)
        {
            logic.GetHit();
            StandardEnemy e = other.GetComponent(typeof(StandardEnemy)) as StandardEnemy;
            e.StateMachine.ChangeState(e.AttackState);
			
			hitSoundEffect.StartCoroutine(hitSoundEffect.PlayerHitSoundEffect());
			enemySounds.PlayEnemyAttackSound(e.Type);
			wizardSounds.PlayFail();
        }
    }
}
