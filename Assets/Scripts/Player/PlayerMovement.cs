﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerMovement : IPausable
{
    private GlobalSettings.GameplaySettings gameplaySettings;
    private Vector3 oldVelocity;

    void Start()
    {
        gameplaySettings = GameObject.FindGameObjectWithTag(GlobalSettings.TAG_GAMEMANAGEMENT).GetComponent<GlobalSettings>().gameplaySettings;
    }

    void Update()
    {
        if (isPaused) return;

        transform.Translate(Vector3.right * Time.deltaTime * gameplaySettings.PlayerMaxSpeed);

        //gameplaySettings.PlayerMaxSpeed += gameplaySettings.LevelAcceleration * Time.deltaTime;
    }

    //    void FixedUpdate()
    //    {
    //        if (isPaused) {
    //			
    //			rigidbody.velocity = Vector3.zero;
    //			return;
    //		}
    //
    //        //rigidbody.AddForce(transform.right * gameplaySettings.PlayerAcceleration);
    //        //rigidbody.AddForce(transform.up * gameplaySettings.PlayerAcceleration);
    //
    //        if (rigidbody.velocity.sqrMagnitude > gameplaySettings.PlayerMaxSpeed * gameplaySettings.PlayerMaxSpeed)
    //        {
    //            rigidbody.velocity = rigidbody.velocity.normalized * gameplaySettings.PlayerMaxSpeed;
    //        }
    //    }
}