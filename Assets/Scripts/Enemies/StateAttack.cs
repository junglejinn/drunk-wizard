﻿using UnityEngine;
using System.Collections;

public class StateAttack : FSMState<BaseEnemy>
{
    public StateAttack(BaseEnemy _owner) : base(_owner) { }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
