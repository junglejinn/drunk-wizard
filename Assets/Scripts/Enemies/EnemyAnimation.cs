﻿using UnityEngine;
using System.Collections;

public class EnemyAnimation : MonoBehaviour {
	
	private Animator anim;	// a reference to the animator on the character
	private AnimatorStateInfo currentBaseState; // a reference to the current state of the animator
	private const string ATTACKING = "Attacking";
	private static int attackState = Animator.StringToHash("Base Layer.Attacking");		
	private static int walkState = Animator.StringToHash("Base Layer.Walking");
	
	
	void FixedUpdate ()
	{
		if (anim != null)
		{
			currentBaseState = anim.GetCurrentAnimatorStateInfo(0);	// set our currentState variable to the current state of the Base Layer (0) of animation
			if (currentBaseState.nameHash == attackState && !anim.IsInTransition(0))
			anim.SetBool(ATTACKING, false);
		}	
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == GlobalSettings.TAG_ENEMY)
		{
			anim = other.GetComponentInChildren<Animator>();
            if( anim != null )
			    anim.SetBool(ATTACKING, true);
		}
	}
}
