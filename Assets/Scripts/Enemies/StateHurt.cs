﻿using UnityEngine;
using System.Collections;

public class StateHurt : FSMState<BaseEnemy>
{
    public StateHurt(BaseEnemy _owner) : base(_owner) { }

    public override void Enter()
    {
        base.Enter();

        owner.Die();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
