﻿using UnityEngine;
using System.Collections;

public class StateIdle : FSMState<BaseEnemy>
{
    public StateIdle(BaseEnemy _owner) : base(_owner) { }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
