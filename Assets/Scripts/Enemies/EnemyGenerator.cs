﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;

public class EnemyGenerator : MonoBehaviour {
	
	/// <summary> Number of enemies to spawn <\summary>
	public int spawnCount = 1;
	/// <summary> List of the different enemy types <\summary>
	public List<GameObject> enemyType;
    /// <summary> Randomize on Y axis </summary>
    public bool GroundUnit = true;
	/// <summary> The spawn circle radius. </summary>
	public float spawnCircleRadius = 3.0f;
	/// <summary> The hack Y offset up or down. </summary>
	public float AdjustSpawnYOffset = 2.0f;
	/// <summary> The enemy container. </summary>
    private EnemyContainer enemyContainer;

    private BackgroundMusic bgm;
	
	void OnDrawGizmos() {
		Gizmos.DrawWireCube(transform.position, transform.localScale);
	}
	
	/// <summary> 
	/// Generates enemies on startup, using the position 
	/// and scale of the object that this script is attached to
	/// <\summary>
	void Start () 
	{
        enemyContainer = EnemyContainer.GetInstance();
        bgm = GameObject.FindObjectOfType(typeof(BackgroundMusic)) as BackgroundMusic;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == GlobalSettings.TAG_PLAYER)
        {
            generateEnemies();
            //GameStateManager.CurrentLevel = int.Parse(name.Substring(name.Length - 1, 1));
            //bgm.EnteredLevel();
        }
    }

    private void generateEnemies()
    {
		Vector3 boundBoxPosition = transform.position;
		Vector3 boundBoxScale    = transform.localScale;

        // Get the X and Y box boundaries
		float minX = boundBoxPosition.x - boundBoxScale.x * 0.5f;
		float maxX = boundBoxPosition.x + boundBoxScale.x * 0.5f;
        float minY = boundBoxPosition.y - boundBoxScale.y * 0.5f;
		float maxY = boundBoxPosition.y + boundBoxScale.y * 0.5f;

        // Calculate the size of the individual spaces that the enemies spawn in
		float diffX = (maxX - minX) / spawnCount;

        // The X,Y coordinates for the previous enemy space 
		float prevX = minX + diffX;
		float prevY = minY + 2.0f;

        // Create spawnCount number of enemies
		for(int i=0; i<spawnCount; i++)
        {
	        // Randomly pick an enemy type
	        GameObject chosenEnemy = enemyType[Mathf.RoundToInt(Random.RandomRange(0, enemyType.Count))];
            
	
	        // Get the size of the enemy
			//Vector3 enemySize = chosenEnemy.GetComponent<MeshFilter>().mesh.bounds.min;
			Vector3 enemySize = chosenEnemy.transform.localScale;
			
	        float sizeY = enemySize.y * 0.5f;
	
	        // Spawn the enemy somewhere in its given space
			Vector2 spawnCircle = Random.insideUnitCircle;
			float newX = prevX + spawnCircle.x * (diffX * 0.5f);// Random.Range(prevX + sizeX, prevX + diffX - sizeX);
			float newY =AdjustSpawnYOffset + prevY + spawnCircle.y * spawnCircleRadius;
			
			if(newX > maxX) break;
			
            // Update prevX/Y with the size of the enemies' spawn space
			prevX = prevX + diffX;

            // The spawn position of the enemy
            Vector2 position;
			//Vector2 spawnCircle = Random.insideUnitCircle * spawnCircleRadius;
            if(!GroundUnit)
			    position = new Vector2(newX, newY);
            else
                position = new Vector2(newX + spawnCircle.x, minY + sizeY);

            GameObject prespawned = enemyContainer.GetEnemies(chosenEnemy);

            // Create the enemy and put it in the scene
            if(prespawned != null)
            {
                Debug.Log("FOUND");
                prespawned.gameObject.transform.position = position;
                prespawned.gameObject.transform.rotation = chosenEnemy.transform.rotation;
                prespawned.gameObject.SetActive(true);
            }
            else 
            {
                Instantiate(chosenEnemy, position, chosenEnemy.transform.rotation);
			}
		}
	}
	
}
