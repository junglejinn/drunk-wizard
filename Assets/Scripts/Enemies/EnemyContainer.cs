﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;

public class EnemyContainer : MonoBehaviour
{
    // Implement as singleton
    private static EnemyContainer instance;
    private static GameObject container;

    public static EnemyContainer GetInstance()
    {
        if (!instance)
        {
            container = new GameObject();
            container.name = "EnemyContainer";
            instance = container.AddComponent(typeof(EnemyContainer)) as EnemyContainer;
        }
        return instance;
    }


    private Dictionary<EnemyType, List<GameObject>> enemyPool = 
			new Dictionary<EnemyType, List<GameObject>>();

    /// <summary>
    /// Add an available enemy to the container
    /// </summary>
    /// <param name="enemy"> Enemy GameObject to add </param>
    public void AddEnemy(GameObject enemy)
    {
		EnemyType type = enemy.GetComponent<StandardEnemy>().Type;
		if(enemyPool.ContainsKey(type))
			enemyPool[type].Add(enemy);
		else 
			enemyPool.Add(type, new List<GameObject>(){enemy});
    }

    /// <summary>
    /// Returns a number of available enemies. 
    /// </summary>
    /// <param name="count"> The number of enemies to get </param>
    /// <returns></returns>
    public GameObject GetEnemies(GameObject enemyType)//, int count)
    {
        EnemyType type = enemyType.GetComponent<StandardEnemy>().Type;
     	if(enemyPool.ContainsKey(type) && enemyPool[type].Count > 0)
		{
			GameObject enemy = enemyPool[type][0];
			enemyPool[type].RemoveAt(0);
			return enemy;
		} 
		else return null;
    }


}


