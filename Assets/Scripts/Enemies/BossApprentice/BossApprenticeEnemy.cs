﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;

public class BossApprenticeEnemy : BaseEnemy
{
    public FSM<BossApprenticeEnemy> StateMachine { get; protected set; }

    public StateApprenticeHurt HurtState { get; protected set; }
    public StateApprenticeAttack AttackState { get; protected set; }
    public StateApprenticeVulnerable VulnerableState { get; protected set; }
	
	public AudioSource bossAudioSource;
	
	// all custom audio clips (which are not in the other enemies)
	public AudioClip ClashSpellAudioClip;
	public AudioClip AttackAudioClip;
	
    public PlayerLogic Player { get { return playerLogic; } }
	
	
    [HideInInspector]
    public GlobalSettings Settings;

    [HideInInspector]
    public Animator animator;

    private GameObject gameManagement;

    private SpellBubbles bubbles;

    private bool startedFight = false;
	
	public EnemySounds newEnemySounds;

    // Use this for initialization
    void Start()
    {
        Type = EnemyType.DarkWizard;

        bubbles = GetComponent<SpellBubbles>();
        playerLogic = GameObject.FindObjectOfType(typeof(PlayerLogic)) as PlayerLogic;
        gameManagement = GameObject.FindGameObjectWithTag(GlobalSettings.TAG_GAMEMANAGEMENT);
        Settings = gameManagement.GetComponent<GlobalSettings>();
        animator = GetComponentInChildren<Animator>();
        Debug.Log(animator);

        gameManagement.GetComponent<PausableManager>().RegisterIPausable(this);

        gameManagement.GetComponent<GameStateManager>().ChangeGameState(GameState.BossBattle);

        HurtState = new StateApprenticeHurt(this);
        AttackState = new StateApprenticeAttack(this);
        VulnerableState = new StateApprenticeVulnerable(this);
        StateMachine = new FSM<BossApprenticeEnemy>();

        playerLogic.MaximumNumberOfSpells = Settings.gameplaySettings.PlayerMaxSpellsInBossFight;
        playerLogic.ClearSpells();
        playerLogic.StopMoving();
    }

    void OnDestroy()
    {
        if (gameManagement)
            gameManagement.GetComponent<PausableManager>().UnregisterIPausable(this);

        GA.API.Design.NewEvent("Player defeated the boss at time (since level was loaded)", Time.timeSinceLevelLoad);
    }
	
	IEnumerable SwitchToPlayerWinScene()
	{
		yield return new WaitForSeconds(1f);
		gameManagement.GetComponent<GameStateManager>().ChangeGameState(GameState.PlayerWin);
	}

    /// <summary>
    /// Prepares spells that have to be imitated by the player
    /// </summary>
    /// <param name="amount">the amount of spells to prepare</param>
    public void PrepareSpells(int amount)
    {
        VulnerableTo.Clear();

        if (amount > 0)
            StartCoroutine(AddSpellDelayed(1, 0, amount));
        else
            bubbles.UpdateSpellBubbles(VulnerableTo);
    }

    private IEnumerator AddSpellDelayed(float t, int index, int amount)
    {
        yield return new WaitForSeconds(t);

        // add a random spell type (could be a combined spell)
        VulnerableTo.Add((SpellType)Random.Range(0, 2));

        bubbles.UpdateSpellBubbles(VulnerableTo);

        if (index < amount - 1)
            StartCoroutine(AddSpellDelayed(t, ++index, amount));
    }

    void Update()
    {
        if (isPaused) return;

        if (!startedFight)
        {
            StateMachine.ChangeState(AttackState);
            startedFight = true;
        }

        StateMachine.Update();
    }

    public void CastSpells()
    {
        playerLogic.GetHit();
        AttackState.Enter();
		
		//EnemySounds newEnemySounds = (EnemySounds)GameObject.FindObjectOfType(typeof(EnemySounds));
		StartCoroutine(PlaySound(AttackAudioClip));    
    }
	
	public IEnumerator PlaySound(AudioClip clip)
	{
		yield return new WaitForSeconds(1);
		
		bossAudioSource.clip = clip;
		bossAudioSource.loop = false;
		bossAudioSource.Play ();
		//if(newEnemySounds)
		//newEnemySounds.PlayEnemyAttackSound(Enumerators.EnemyType.DarkWizard);
	}
	
	 void OnBecameVisible()
    {
		if(newEnemySounds)
        newEnemySounds.PlayEnemySound(Enumerators.EnemyType.DarkWizard);
    }

    public override bool Attacked(List<SpellType> spells)
    {
        if (StateMachine.CurrentState == VulnerableState)
        {
            Die();
            return true;
        }

        if (VulnerableTo.Count != spells.Count) return false;

        // remove matching type
        int count = VulnerableTo.Count;
        for (int i = 0; i < VulnerableTo.Count; i++)
        {
            if (spells[i] != VulnerableTo[i]) return false;
            else
            {
                count--;
            }
        }

        // interrupt attacking, get hurt
        if (count == 0)
        {
            StateMachine.ChangeState(HurtState);
        }

        return true;
    }

    public IEnumerator StopAnimationWhenFinished(string name)
    {
        AnimatorStateInfo state;
        if (!(state = animator.GetCurrentAnimatorStateInfo(0)).IsName(name))
            yield return null;

        yield return new WaitForSeconds(state.length);

        animator.SetBool(name, false);
    }

    void OnMouseDown()
    {
        playerLogic.ShootSpells(this);
    }

    public override void Die()
    {
        playerLogic.MaximumNumberOfSpells = Settings.gameplaySettings.PlayerMaxSpells;
        playerLogic.ClearSpells();

		StartCoroutine(DelayedDeath());
    }
	
	IEnumerator DelayedDeath()
	{
		yield return new WaitForSeconds(1.5f);
		GameObject.FindGameObjectWithTag(GlobalSettings.TAG_GAMEMANAGEMENT).GetComponent<GameStateManager>().ChangeGameState(GameState.PlayerWin);
		Destroy(gameObject);
	}
}
