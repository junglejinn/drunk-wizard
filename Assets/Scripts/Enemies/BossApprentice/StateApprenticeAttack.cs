﻿using UnityEngine;
using System.Collections;

/// <summary>
/// In this state the enemy will repeatedly attack the player, except if the player interrupts it by countering the spells
/// </summary>
public class StateApprenticeAttack : FSMState<BossApprenticeEnemy>
{
    private int currentAmountOfSpells;
    private float timeToAttack;

    private bool attackedPlayer;

    public StateApprenticeAttack(BossApprenticeEnemy _owner) : base(_owner)
    {
        currentAmountOfSpells = owner.Settings.gameplaySettings.BossStartingSpellAmount; 
    }

    public override void Enter()
    {
        base.Enter();

        owner.PrepareSpells(currentAmountOfSpells);
        currentAmountOfSpells++;

        timeToAttack = owner.Settings.gameplaySettings.BossAttackTimer;

        attackedPlayer = false;
    }

    public override void Update()
    {
        base.Update();

        timeToAttack -= Time.deltaTime;

        if (timeToAttack <= 0)
        {
            owner.Player.QueuePlayerForGettingHit(owner);
            Reset();
            owner.CastSpells();
            owner.animator.SetBool("Attacking", true);
            owner.StartCoroutine(owner.StopAnimationWhenFinished("Attacking"));
            attackedPlayer = true;
        }
    }

    public void Reset()
    {
        currentAmountOfSpells = owner.Settings.gameplaySettings.BossStartingSpellAmount;
        timeToAttack = owner.Settings.gameplaySettings.BossAttackTimer;
    }

    public override void Exit()
    {
        base.Exit();

        if (!attackedPlayer) // this check is kind of useless, since the enemy should not exit the attackstate unless he's interrupted
        {
            // play animation or particles or effect or something to show that he was interrupted
            owner.Player.QueuePlayerBossClash(owner);
			owner.StartCoroutine(owner.PlaySound(owner.ClashSpellAudioClip));
        }
    }
}
