﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This state is used to keep track of how many attacks are needed until the enemy becomes vulnerable
/// </summary>
public class StateApprenticeHurt : FSMState<BossApprenticeEnemy>
{
    public StateApprenticeHurt(BossApprenticeEnemy _owner) : base(_owner) 
    {
    }

    public override void Enter()
    {
        base.Enter();

        owner.animator.SetBool("Hit", true);
        owner.StartCoroutine(owner.StopAnimationWhenFinished("Hit"));

        if (owner.VulnerableTo.Count >= owner.Settings.gameplaySettings.BossMaxSpellsToImitate)
        {
            owner.StateMachine.ChangeState(owner.VulnerableState);
        }
    }

    public override void Update()
    {
        base.Update();

        owner.StateMachine.ChangeState(owner.AttackState);
    }

    public override void Exit()
    {
        base.Exit();
    }
}
