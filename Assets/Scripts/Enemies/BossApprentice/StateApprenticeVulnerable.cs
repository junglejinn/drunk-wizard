﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This state is entered when the enemy is vulnerable and can be killed by anything
/// </summary>
public class StateApprenticeVulnerable : FSMState<BossApprenticeEnemy>
{
    private float timeLeft;

    public StateApprenticeVulnerable(BossApprenticeEnemy _owner) : base(_owner) { }

    public override void Enter()
    {
        base.Enter();

       // timeLeft = owner.Settings.gameplaySettings.EnemyApprenticeStunnedTime;

        owner.PrepareSpells(0);
 
        //owner.AttackState.Reset();
    }

    public override void Update()
    {
        base.Update();

        //timeLeft -= Time.deltaTime;
        
        //if (timeLeft <= 0)
        //    owner.StateMachine.ChangeState(owner.AttackState);
    }

    public override void Exit()
    {
        base.Exit();
    }
}
