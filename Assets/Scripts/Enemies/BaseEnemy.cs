﻿using UnityEngine;
using System.Collections;
using Enumerators;
using System.Collections.Generic;

public abstract class BaseEnemy : IPausable
{
    public List<SpellType> VulnerableTo;

    public EnemyType Type;

    protected PlayerLogic playerLogic;
    protected EnemySounds enemySounds;
    protected GameObject player;

    void OnBecameInvisible()
    {
        if (enemySounds)
            enemySounds.StopEnemySound();
    }

	void OnBecameVisible()
    {
        if (enemySounds)
            enemySounds.PlayEnemySound(Type);
    }
	
    // Use this for initialization
    void Start()
    {
		
        
    }
	
	void Awake()
	{
		enemySounds = (EnemySounds)GameObject.FindObjectOfType(typeof(EnemySounds));
		playerLogic = GameObject.FindObjectOfType(typeof(PlayerLogic)) as PlayerLogic;
		gameObject.AddComponent("MeshRenderer");
	}

    public abstract bool Attacked(List<SpellType> spells);

    public virtual void Die()
    {
		enemySounds.PlayDieSound(Type);
        EnemyContainer.GetInstance().AddEnemy(gameObject);
        gameObject.SetActive(false);
    }

    void OnMouseDown()
    {
        // TODO: quickfix for null ref
        playerLogic.ShootSpells(this);
    }
}
