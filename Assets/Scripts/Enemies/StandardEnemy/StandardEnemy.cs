﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;

public class StandardEnemy : BaseEnemy
{
    public FSM<BaseEnemy> StateMachine { get; protected set; }

    public StateIdle IdleState { get; private set; }
    public StateHurt HurtState { get; private set; }
    public StateAttack AttackState { get; private set; }

    private float change = 0.5f;
	private List<SpellType> temp;
	
    // Use this for initialization
    void Start()
    {
        IdleState = new StateIdle(this);
        HurtState = new StateHurt(this);
        AttackState = new StateAttack(this);
        StateMachine = new FSM<BaseEnemy>();
        StateMachine.ChangeState(IdleState);
		temp = new List<SpellType>();
		for(int i = 0; i < VulnerableTo.Count; i++)
        {
			temp.Add(VulnerableTo[i]);
		}
    }
	
	void OnEnable() {
		if(temp != null){
			for(int i = 0; i < VulnerableTo.Count; i++){
				temp.Add(VulnerableTo[i]);
			}
			
		}
		if(StateMachine != null) {
			StateMachine.ChangeState(IdleState);
		} else {
			StateMachine = new FSM<BaseEnemy>();	
			StateMachine.ChangeState(IdleState);
		}
	}

    public override bool Attacked(List<SpellType> spells)
    {
        //foreach(SpellType s in temp) {
        //    Debug.Log(s);
        //}
		
		if(temp.Count == 0 || spells.Count == 0){
			return false;
		}
           
        //Debug.Log(spells[0] + " " + temp[0]);
        
		if (spells[0] != temp[0])
		{
            //Debug.Log("EPIC FAIL!");
			return false;
		}
        else
        {
			
            //Debug.Log ("KILL" + spells[0]);
            temp.Remove(spells[0]);
        }

        if (temp.Count == 0)
        {
            StateMachine.ChangeState(HurtState);
        }

        return true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPaused) return;

        StateMachine.Update();
		
		UpdatePosition ();
    }
	

	
    //Lowers the enemy to the same height as the player.
    public void UpdatePosition()
    {
		if( player == null) {
			player = GameObject.Find("Wizard");
		}
        float playerPositionX = player.transform.position.x + 10;
        float enemyPositionX = transform.position.x;
        float enemyPositionY = transform.position.y;
        float deltaX = Mathf.Abs(playerPositionX - enemyPositionX);

        if (change * deltaX <= enemyPositionY)
        {
            transform.position = new Vector3(transform.position.x, change * deltaX, transform.position.z);
        }

  	}
	
	void OnTriggerEnter(Collider c)
	{
		if(c.tag == GlobalSettings.TAG_DESPAWN) {
			Die();
		}
	}
	
}
