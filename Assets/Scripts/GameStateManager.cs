﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;

public class GameStateManager : MonoBehaviour
{
	/// <summary> The GameState you want to start up in, if any. </summary>
	public GameState startingGameState = GameState.None;
	/// <summary> The GameObject used as the deathScreen </summary> 
	public GameObject DeathScreen;
	/// <summary> The current state of the game.</summary>
    public static GameState CurrentState { get; private set; }
    public static int CurrentLevel = -1;
	
	/// <summary>
	/// ALL Objects instantiated via this class should be added to this list. The list is cleared whenever
	/// GameState.RunningGame is called to make sure all overlayed objects are removed.
	/// </summary>
	private List<GameObject> instantiatedObjects = new List<GameObject>();
    private PausableManager pauseableManager;
    private PlayerLogic playerLogic;

    void Start()
    {
        pauseableManager = transform.GetComponent<PausableManager>();
        playerLogic = GameObject.FindObjectOfType(typeof(PlayerLogic)) as PlayerLogic;
		
		if (startingGameState != GameState.None)
        	ChangeGameState(startingGameState);
    }

    public void ChangeGameState(GameState gameState)
    {
		Debug.Log("STATE CHANGED TO: " + gameState);
        CurrentState = gameState;
        switch (gameState)
        {
            case GameState.RunningGame:			
                // Remove whatever GameObjects have been instantiated.
				instantiatedObjects.Clear();
                if (pauseableManager) pauseableManager.ResumeGame();
                if (playerLogic)
                    playerLogic.StartMoving();
                break;
            case GameState.PausedGame:
                pauseableManager.PauseGame();
                break;
            case GameState.BossBattle:
                playerLogic.StopMoving();
                break;
            case GameState.Quit:
                Application.Quit();
				break;
        	case GameState.PlayerDeath:
				pauseableManager.PauseGame();
				if (DeathScreen != null)
					DeathScreen.SetActive(true);
				break;
			case GameState.MainMenu:
				Application.LoadLevel("Wizard-Menu"); // Where the integer corresponds to the MainMenu.
				break;
			case GameState.SpellBook:
				break;
			case GameState.PlayerWin:
				Application.LoadLevel("Wizard-Outro");
				break;
            default:
                break;
        }
    }
}