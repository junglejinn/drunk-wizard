﻿using UnityEngine;
using System.Collections;

public class GlobalSettings : MonoBehaviour
{
    public GameplaySettings gameplaySettings;

    // TAGS (to avoid typos)
    public const string TAG_GAMEMANAGEMENT = "GameManagement";
    public const string TAG_PLAYER = "Player";
    public const string TAG_PLAYERHEARTS = "PlayerHearts";
    public const string TAG_ENEMY = "Enemy";
	public const string TAG_LIGHT = "Light";
	public const string TAG_DESPAWN = "Despawn";

    /// <summary>
    /// A class holding all gameplay settings
    /// </summary>
    [System.Serializable]
    public class GameplaySettings
    {
        public float PlayerMaxSpeed = 10;
        public int PlayerMaxSpellsInBossFight = 5;
        public int PlayerMaxSpells = 2;
        public float EnemyApprenticeStunnedTime = 3;
        public float BossAttackTimer = 5;
        public int BossMaxSpellsToImitate = 5;
        public int BossStartingSpellAmount = 3;
		public float BossDeathTime = 1.0f;
    }
	
	void Start()
	{
		Screen.sleepTimeout = (int) SleepTimeout.NeverSleep;
	}
}