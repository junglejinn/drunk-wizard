﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;

public class ParticleSpawner : MonoBehaviour {
		
	[System.Serializable]
	public class Particles{
		public GameObject fire;
		public GameObject water;
		public GameObject lightning;
		public GameObject thunderstorm;
		public GameObject fizzle;
		public GameObject clashWater;
		public GameObject clashLightning;
		public GameObject clashThunderstorm;
		
	}
	public float timeToLive = 5.0f;
	public Particles spellParticles;
	public float verticalOffsetProblem = 6;
	//Player playerScript;
	//GameObject chosenSpellParticles;
	
	// Use this for initialization
	void Start () {
	
		//playerScript = GetComponent<Player>();
		
	}
	
	 

	//instantiate the correct particle emitter prefab, according to what spell was requested, and between which points.
	public void SpawnParticles(bool clash, bool didHit, List<SpellType> spellsToCast, Transform target){
		
		
		GameObject chosenSpellParticles;// = new GameObject();
		//Vector3 posish = target.position;
		
		//foreach(SpellType spellType in spellsToCast){
		SpellType spellType = spellsToCast[0];
		
		
		
			//Debug.Log ("SPAWN PARTICLES");
			if(!didHit || spellType == SpellType.Failure){
				//some kinda fizzle
			
				chosenSpellParticles = (GameObject)Instantiate(spellParticles.fizzle, target.position, Quaternion.identity); 
			}else
			if(spellType == SpellType.Fire){
				//chosenSpellParticles = (GameObject)Instantiate(spellParticles.fire, new Vector3(target.position.x, target.position.y+verticalOffsetProblem, target.position.x), Quaternion.identity); 
				
				chosenSpellParticles = (GameObject)Instantiate(spellParticles.fire, target.position, Quaternion.identity); 
				//Debug.Log ("spawning fire");
			}else if(spellType == SpellType.Water){
				//chosenSpellParticles = (GameObject)Instantiate(spellParticles.water, new Vector3(target.position.x, target.position.y+verticalOffsetProblem, target.position.x), Quaternion.identity); 
				//TODO: needs to know if spell is clash or not; instead of whether is in boss battle mode.
				if(clash){
					chosenSpellParticles = (GameObject)Instantiate(spellParticles.clashWater, target.position, Quaternion.identity);
				}else
					chosenSpellParticles = (GameObject)Instantiate(spellParticles.water, target.position, Quaternion.identity); 
				//Debug.Log ("spawning water");
			}else if(spellType == SpellType.Lightning){
				//Debug.Log ("spawning lightning");
				//chosenSpellParticles = (GameObject)Instantiate(spellParticles.lightning, new Vector3(target.position.x, target.position.y+verticalOffsetProblem, target.position.x), Quaternion.identity); 	
				if(clash){
			
					chosenSpellParticles = (GameObject)Instantiate(spellParticles.clashLightning, target.position, Quaternion.identity);
				}else
					chosenSpellParticles = (GameObject)Instantiate(spellParticles.lightning, target.position, Quaternion.identity); 	
			}else if(spellType == SpellType.ThunderStorm){
				//spawn both lightning and water.
				//chosenSpellParticles = (GameObject)Instantiate(spellParticles.thunderstorm, new Vector3(target.position.x, target.position.y+verticalOffsetProblem, target.position.x), Quaternion.identity); 
				if(clash){
					chosenSpellParticles = (GameObject)Instantiate(spellParticles.clashThunderstorm, target.position, Quaternion.identity);
				}else
					chosenSpellParticles = (GameObject)Instantiate(spellParticles.thunderstorm, target.position, Quaternion.identity); 
			}else 
				chosenSpellParticles = new GameObject();
			
			//Debug.Log("----> "+target.tag);
			//chosenSpellParticles.transform.LookAt(targetPosition);
			chosenSpellParticles.transform.rotation = transform.rotation;
			
			//chosenSpellParticles.transform.parent = transform;
			
			//new Vector3(target.position.x, target.collider.bounds.center.y, target.position.x)
			//target.collider.transform.TransformPoint(target.collider.);
			
			
			StartCoroutine(DespawnParticles(chosenSpellParticles));
		//}
	}
		
	//Manually destroy particles. (Note: they will die on their own when their time runs out, based on the time var)
	public IEnumerator DespawnParticles(GameObject chosenSpellParticles){
		
		//yield return new WaitForSeconds((float)playerScript.spellFxLifetime);
		yield return new WaitForSeconds(timeToLive);
		
		//Debug.Log ("despawning spell particles.");
		//DespawnParticles();
		//if(chosenSpellParticles)
			Destroy(chosenSpellParticles);
		
	}
	
	
	// Update is called once per frame
	void Update () {
		
		
	
	}
	
}
