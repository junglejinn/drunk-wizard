﻿using UnityEngine;
using System.Collections;

public class ClashParticleRotateAndStretch : MonoBehaviour {

	private GameObject player;
	private Vector3 dir;
	private Camera mainCamera;
	private Vector3 originalScale;
	//private Vector3 startingDistace;
	// Use this for initialization
	void Start () {
		PlayerControl p = GameObject.FindObjectOfType(typeof(PlayerControl)) as PlayerControl;
		player = p.spellCastOrigin;
		//player = GameObject.FindGameObjectWithTag("Player").transform.GetChild(0).gameObject;
		originalScale = transform.localScale;
		//startingDistace = transform.position;
	}
	
	
	// Update is called once per frame
	void Update () {
		
		//dir = player.transform.position.x < transform.position.x ? (player.transform.position - transform.position) : 
		//														   (transform.position - player.transform.position);//.normalized;
		//dir.x = -player.transform.rotation.z;
		
		if(player.transform.position.x < transform.position.x){
			dir = player.transform.position - transform.position;
			transform.rotation = Quaternion.LookRotation(dir);
		}else{
			dir = transform.position - player.transform.position;
			transform.rotation = Quaternion.LookRotation(dir);
			transform.Rotate(180,0,0);
		}
		
		
		
		
		float currentDist = Vector3.Distance(player.transform.position, transform.position)/10;
		//float offset = Vector3.Distance(transform.position, player.transform.position);//player.transform.position - transform.position;
		//transform.localScale.Scale(new Vector3(transform.localScale.x , transform.localScale.y, offset));
	
		transform.localScale = new Vector3(transform.localScale.x , transform.localScale.y, originalScale.z*currentDist);
		//transform.position.
		
		//Debug.Log("originalScale.z: "+originalScale.z+"; currentDist: "+currentDist+"; (originalScale.z*currentDist): "+(originalScale.z*currentDist));
	}
}
