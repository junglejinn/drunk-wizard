﻿using UnityEngine;
using System.Collections;

public class ClashOffset : MonoBehaviour {
	
	private GameObject player;
	// Use this for initialization
	void Start () {
		PlayerControl p = GameObject.FindObjectOfType(typeof(PlayerControl)) as PlayerControl;
		player = p.spellCastOrigin;
		//player = GameObject.FindGameObjectWithTag("Player");
		transform.position = new Vector3( transform.position.x + (player.transform.position.x - transform.position.x)/2, transform.position.y + (player.transform.position.y - transform.position.y)/2, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
