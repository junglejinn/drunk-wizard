﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;

[System.Serializable]
public class SpellCombination
{
    public List<SpellType> InputSpells;
    public SpellType OutputSpell;
}
