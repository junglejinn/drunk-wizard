﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enumerators;

public class SpellConverter : MonoBehaviour
{
    /// <summary>
    /// All possible spell combinations
    /// </summary>
    public List<SpellCombination> SpellCombinations;

    /// <summary>
    /// combinations ordered by output type
    /// </summary>
    private Dictionary<SpellType, List<SpellType>> combinationSpellDict;

    void Start()
    {
        combinationSpellDict = new Dictionary<SpellType, List<SpellType>>();

        foreach (SpellCombination c in SpellCombinations)
        {
            combinationSpellDict.Add(c.OutputSpell, c.InputSpells);
        }
    }

    /// <summary>
    /// Converts a list of input spell types into the according output spell type
    /// </summary>
    public List<SpellType> Convert(List<SpellType> inputSpells)
    {
        List<SpellType> result = new List<SpellType>();
        List<SpellType> testRange;

        // go through all spells the player has currently equipped
        for (int leftIndex = 0; leftIndex < inputSpells.Count; )
        {
            bool foundCombination = false;

            // check all combinations if the next view spells from the inputspells create
            // a new combination of spells
            foreach (SpellCombination c in SpellCombinations)
            {
                // check if the next combination fits into the inputspell list
                if (leftIndex + c.InputSpells.Count - 1 < inputSpells.Count)
                    testRange = inputSpells.GetRange(leftIndex, c.InputSpells.Count);
                else
                    break;

                // check if the current combination has the same input spells as the player
                // has equipped
                if (ScrambledEquals<SpellType>(testRange, c.InputSpells) == true)
                {
                    result.Add(c.OutputSpell);
                    leftIndex += c.InputSpells.Count;
                    foundCombination = true;
                    break;
                }
            }

            // if no combination was found, add the current spell and move the index pointer one to the right
            if (!foundCombination)
            {
                result.Add(inputSpells[leftIndex]);
                leftIndex++;
            }
        }

        return result;
    }

    public List<SpellType> GetInputSpells(SpellType combinedSpellType)
    {
        List<SpellType> result;
        combinationSpellDict.TryGetValue(combinedSpellType, out result);

        return result;
    }

    //ref: http://stackoverflow.com/questions/3669970/compare-two-listt-objects-for-equality-ignoring-order
    /// <summary>
    /// Checks if two unordered lists contain the same items
    /// </summary>
    /// <returns>if the two lists contain the same items</returns>
    public static bool ScrambledEquals<T>(IEnumerable<T> list1, IEnumerable<T> list2)
    {
        // dictionary with counts for each item
        var cnt = new Dictionary<T, int>();
        foreach (T s in list1)
        {
            if (cnt.ContainsKey(s))
            {
                cnt[s]++;
            }
            else
            {
                cnt.Add(s, 1);
            }
        }
        foreach (T s in list2)
        {
            if (cnt.ContainsKey(s))
            {
                cnt[s]--;
            }
            else
            {
                return false;
            }
        }

        bool same = true;

        foreach (int v in cnt.Values)
        {
            if (v != 0) return false;
        }

        return same;
    }
}
