﻿using UnityEngine;
using System.Collections;

public class FSM<T>
{
    /// <summary>
    /// The Finite State Machine's current state
    /// </summary>
    public FSMState<T> CurrentState { get; set; }
    /// <summary>
    /// The Finite State Machine's last state
    /// </summary>
    public FSMState<T> LastState { get; set; }

    /// <summary>
    /// Changes the state
    /// </summary>
    /// <param name="newState">The state to be changed to</param>
    /// <returns>if the state was changed</returns>
    public bool ChangeState(FSMState<T> newState)
    {
        if (newState != null && newState != CurrentState)
        {
            LastState = CurrentState;
            if (LastState != null) { LastState.Exit(); }

            CurrentState = newState;
            CurrentState.Enter();
            return true;
        }
        return false;
    }

    /// <summary>
    /// Updates the current state
    /// </summary>
    public void Update()
    {
        CurrentState.Update();
    }
}
