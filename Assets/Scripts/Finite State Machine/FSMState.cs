﻿using UnityEngine;
using System.Collections;

public class FSMState<T>
{
    protected T owner;

    public FSMState(T _owner)
    {
        owner = _owner;
    }

    public virtual void Enter()
    {
    }

    public virtual void Update()
    {

    }

    public virtual void Exit()
    {

    }
}
