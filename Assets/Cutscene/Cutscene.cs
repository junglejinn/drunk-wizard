﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cutscene : MonoBehaviour {

    public List<Texture> pages = new List<Texture>();
    public float waitPerFlip = 5.0f;
	
	// Use this for initialization
	void Start () {		
        StartCoroutine(pageFlip());
	}
	
    IEnumerator pageFlip() {
        foreach (Texture page in pages)
        {
            renderer.materials[0].mainTexture = page;
            yield return new WaitForSeconds(waitPerFlip);
        }
        Application.LoadLevel("Wizard-Menu");
        
	}
	
	void OnMouseDown()
	{
		Application.LoadLevel("Wizard-Menu");
	}




}
