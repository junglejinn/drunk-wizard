﻿using UnityEngine;
using System.Collections;

public class FizzleScript : MonoBehaviour {

	
	public float smokeDelay = 1.0f;
	public float loopduration = 1.25f;
	public float loopspeed = 2.0f;
	public Vector3 rangeMaxEndValue = new Vector3(0.35f, 0.5f,0);
	public float clipRangeValue = 0.5f;
	public GameObject ExplosionSmokePrticles;
	private GameObject smokeChildParticles1;
	private GameObject smokeChildParticles2;
	
	private GameObject explosionSmoke;
	
	private float startFadingAfter;// = 2.0f;
	private float startClippingAfter;// = 2.0f;
	
	private float t=0.0f;
	//private float growT=0.0f;
	private float clipT=0.0f;
	private float startTime;
	
	private Vector3 rangeMaxStartValue;
	private float clipRangeStartValue;
	
	private Vector2 rangeLerpContainer;
	private float clipRangeLerpContainer;
	
	
	//private Vector3 originalSize;
	
	//NOTE: this shader does not stop itself after the animation is done. It becomes invisible but is still processed.
	//This shouldn't be a problem as the object it is used on is destroyed when the animation is done.
	
	// Use this for initialization
	void Start () {
	
		rangeMaxStartValue = renderer.material.GetVector("_Range");
		clipRangeStartValue = renderer.material.GetFloat("_ClipRange");
		
		startTime = Time.time;
		
		//originalSize = transform.localScale;
		
		startFadingAfter = loopduration/2;
		startClippingAfter = loopduration/2;
		
		StartCoroutine(spawnSmoke(smokeDelay));
		
		smokeChildParticles1 = ExplosionSmokePrticles.transform.GetChild(0).gameObject;
		smokeChildParticles2 = ExplosionSmokePrticles.transform.GetChild(1).gameObject;
		
		//explosionSmoke.transform.localScale = transform.localScale;
		float buff = 4;
		float buff2 = 1.3f;
		//smokeChildParticles1.transform.localScale = transform.localScale /buff;
		smokeChildParticles1.particleSystem.startSize = transform.lossyScale.x *buff2*1.25f;
		smokeChildParticles1.transform.localScale = transform.localScale /buff*1.25f;
		
		//smokeChildParticles2.transform.localScale = transform.localScale /buff;
		smokeChildParticles2.particleSystem.startSize = transform.lossyScale.x *buff2*1.25f;
		smokeChildParticles2.transform.localScale = transform.localScale /buff*1.25f;
	}
	
	// Update is called once per frame
	void Update () {
		//Animating the displacement; must be attached to the explosion object

		float r = Mathf.Sin((Time.time / loopspeed) * (2 * Mathf.PI)) * 0.5f + 0.25f;
		float g = Mathf.Sin((Time.time / loopspeed + 0.33333333f) * 2 * Mathf.PI) * 0.5f + 0.25f;
		float b = Mathf.Sin((Time.time / loopspeed + 0.66666667f) * 2 * Mathf.PI) * 0.5f + 0.25f;
		float correction = 1 / (r + g + b);
		r *= correction;
		g *= correction;
		b *= correction;
		renderer.material.SetVector("_ChannelFactor", new Vector4(r,g,b,0));
		
		float currentDelta = Time.deltaTime/loopduration;
		if(Time.time - startTime > startFadingAfter && t < 1){
		    t += currentDelta;
		}
		if(Time.time - startTime > startClippingAfter && clipT < 1){	
			clipT += currentDelta;
		}
		
		rangeLerpContainer = Vector2.Lerp(new Vector2(rangeMaxStartValue.x, rangeMaxStartValue.y), new Vector2(rangeMaxEndValue.x, rangeMaxEndValue.y), t);
		//Debug.Log("rangeMaxStartValue: "+rangeMaxStartValue+"; rangeMaxEndValue: "+rangeMaxEndValue+"; rangeLerpContainer: "+rangeLerpContainer+"; loopduration: "+loopduration);
		renderer.material.SetVector("_Range", new Vector3(rangeLerpContainer.x,
														  rangeLerpContainer.y,
														  rangeMaxStartValue.z));
		
		clipRangeLerpContainer = Mathf.Lerp(clipRangeStartValue, clipRangeValue, clipT);
		//Debug.Log("clipRangeLerpContainer: "+clipRangeLerpContainer);
		renderer.material.SetFloat("_ClipRange", clipRangeLerpContainer);
		
		/*
		if(growT < 1){	
			growT += currentDelta;
		}
		transform.localScale = Vector3.Lerp(Vector3.zero, originalSize, growT);
		*/
		
		
	}
	
	private IEnumerator spawnSmoke(float seconds){
	
		yield return new WaitForSeconds(seconds);
		
		explosionSmoke = (GameObject)Instantiate(ExplosionSmokePrticles, transform.position, Quaternion.identity);
		
		
		
		explosionSmoke.transform.parent = transform.parent;
		
		//yield return new WaitForSeconds(1);
		//Debug.Log ("Destroying particles");
		//Destroy(explosionSmoke);
	}
}
