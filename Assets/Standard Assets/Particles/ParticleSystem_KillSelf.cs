﻿using UnityEngine;
using System.Collections;

public class ParticleSystem_KillSelf : MonoBehaviour {
	
	public float TimeToLive = 5.0f;
	
	// Use this for initialization
	void Start () {
		//StartCoroutine(destroySelf(TimeToLive));
		destroySelf(TimeToLive);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	/*
	public IEnumerator destroySelf(float seconds){
	
		yield return new WaitForSeconds(seconds);
		Destroy(transform);
	}
	*/
	public void destroySelf(float seconds){
	
		Destroy(gameObject, seconds);
	}
}
