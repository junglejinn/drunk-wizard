﻿using UnityEngine;
using System.Collections;

public class TestExplosionScript : MonoBehaviour {
	
	public GameObject ExplosionPrefab;
	private GameObject currentExplosion;
	private GameObject newExplosion;
	private float prevTime;
	public float length = 1.25f;
	
	// Use this for initialization
	void Start () {
		prevTime = 0;
		currentExplosion = (GameObject)Instantiate(ExplosionPrefab, transform.position, Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		
		//Debug.Log("Time.time - prevTime: "+(Time.time - prevTime)+"; prevTime = "+prevTime);
		if(Time.time - prevTime > length){
			prevTime = Time.time;
			Debug.Log("spawning");
			Destroy(currentExplosion);
			//currentExplosion = (GameObject)Instantiate(ExplosionPrefab, new Vector3(transform.position.x, transform.position.y+4, transform.position.z), Quaternion.identity);
			currentExplosion = (GameObject)Instantiate(ExplosionPrefab, transform.position, Quaternion.identity);
			
		}
	}
}
